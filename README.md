# ANPDB Galaxy


## Getting started

- Install the [Galaxy](https://galaxyproject.org/admin/get-galaxy/#for-development)

```
git clone https://github.com/galaxyproject/galaxy.git
cd galaxy
sh run.sh
```
- Required tools have to be added to the tools folder.[Follow these instructions](https://galaxyproject.org/admin/tools/add-tool-tutorial/#2-put-the-tool-into-galaxys-tools-directory) for the folder Fetch_smiles and Navigate. Put them in same newly created directory(e.g myTools).
Follow the steps mentioned below:
Now, in the new terminal: 
```
cd tools
```
Now, clone the tool in ANPDB folder.

```
git clone git@gitlab.com:usegalaxy-in/anpdb-galaxy.git
```
To make sure the tools run, 
Run the setup.pl script in the Fetch_Smiles folder to install dependencies.
```
cd anpdb-galaxy/Fetch_Smiles/
chmod +x setup.pl

perl setup.pl
```


Now, add the tool in the configuration. 
- For tool_conf.xml.sample in the config folder, add this: 
```
If you are in the galaxy folder location(check with pwd command):
cd config
vim tool_conf.xml.sample

Add this as separate section: 
 <section name="ANPDB" id="ANPDB">
    <tool file="anpdb-galaxy/Navigate/fetchANPDB.xml" />
    <tool file="anpdb-galaxy/Fetch_Smiles/fetchANPDB1.xml" />
 </section>
```
Make sure the paths are correct. Change as required.

Now restart the script
```
sh run.sh --restart
sh run.sh
```

## Required compound files on the African compounds website

- Link to the [**SMI**](https://african-compounds.org/anpdb/downloads/) compounds of ANDPDB


## Help

- If you have questions or require help please drop a message in [gitter adda (अड्डा)](https://gitter.im/usegalaxy-in/adda)
