#!/usr/bin/perl

## Uses compound names to fetch SMILES structures from ANPDB

use strict;
use warnings;

# Check if required modules are available
eval {
    require LWP::Simple;
    LWP::Simple->import();
    1;
} or die "LWP::Simple module is required. Install it using 'cpan LWP::Simple'\n";

eval {
    require HTML::TreeBuilder;
    HTML::TreeBuilder->import();
    1;
} or die "HTML::TreeBuilder module is required. Install it using 'cpan HTML::TreeBuilder'\n";

# Check for required arguments
if (@ARGV < 1) {
    die "Usage: $0 <compound_name> [pubchem_cid]\n";
}

my $compound_name = $ARGV[0];
my $pubchem_cid = $ARGV[1];

sub fetch_and_parse_page {
    my ($page) = @_;
    my $url = "https://african-compounds.org/anpdb/compounds_list/?page=$page&q=All";

    my $content = get($url);

    unless (defined $content) {
        die "Error fetching data from ANPDB\n";
    }

    my $tree = HTML::TreeBuilder->new_from_content($content);

    foreach my $link ($tree->find('a')) {
        my $compound_url = $link->attr('href');
        my $compound_name_link = $link->as_text;

        if ($compound_url =~ /\/anpdb\/get_compound_card\/(\d+)/) {
            if ((defined $compound_name && $compound_name_link =~ /\Q$compound_name\E/i) ||
                (defined $pubchem_cid && $compound_name_link =~ /\Q$pubchem_cid\E/i)) {

                my $card_url = "https://african-compounds.org$compound_url";

                my $card_content = get($card_url);
                unless (defined $card_content) {
                    warn "Error fetching compound card page for $compound_name_link\n";
                    next;
                }

                my $card_tree = HTML::TreeBuilder->new_from_content($card_content);

                my ($smiles) = $card_tree->as_text =~ /SMILES:\s*(.+)\s*InChI:/s;

                print "$smiles\n";

                $card_tree->delete;
                return 1;
            }
        }
    }

    return 0;
}

for my $page (1 .. 150) {
    if (fetch_and_parse_page($page)) {
        exit;
    }
}

print "Compound not found\n";
