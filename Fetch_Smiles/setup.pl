#!/usr/bin/perl

use strict;
use warnings;

# Function to install CPAN
sub install_cpan {
    my $distro = shift;
    
    if ($distro =~ /ubuntu|debian/i) {
        system("sudo apt-get update && sudo apt-get install cpanminus");
    } elsif ($distro =~ /fedora|centos|redhat/i) {
        system("sudo dnf install perl-App-cpanminus");
    } elsif ($distro =~ /arch/i) {
        system("sudo pacman -Syu && sudo pacman -S --noconfirm perl-cpanminus");
    } elsif ($distro =~ /opensuse/i) {
        system("sudo zypper install perl-App-cpanminus");
    } else {
        die "Unsupported Linux distribution: $distro\n";
    }
}

# Detect Linux distribution
open my $fh, '<', '/etc/os-release' or die "Could not open /etc/os-release: $!";
my $distro;
while (<$fh>) {
    if (/^ID=(.*)/) {
        $distro = $1;
        last;
    }
}
close $fh;

# Install CPAN if not installed
my $cpan_installed = system("cpan --version > /dev/null 2>&1") == 0;
unless ($cpan_installed) {
    print "CPAN not found. Installing CPAN...\n";
    install_cpan($distro);
}

# Check if the required modules are installed
my @modules = ('LWP::Simple', 'HTML::TreeBuilder');

foreach my $module (@modules) {
    eval "use $module";
    if ($@) {
        print "Installing $module...\n";
        system("cpanm $module");
    } else {
        print "$module is already installed.\n";
    }
}
